require "spec_helper"
require './app'
require './models/shortner'

RSpec.describe "App Requests" do
  def app
    Sinatra::Application
  end

  describe "/shorten" do
    context "WHEN there are no errors" do
      it "should return 201 and the shortcode submitted" do
        shortcode = Shortner.gernerate_code
        params = {
          url: "https://www.google.com/",
          shortcode: shortcode,
          format: :json
        }
        headers = {'Content-Type' => 'application/json'}
        post "/shorten", params.to_json, headers
        expect(last_response.status).to eq 201
        json_response = JSON.parse(last_response.body)
        expect(json_response["shortcode"]).to be == shortcode
      end

      it "should return 201 and a generated shortcode that mattches the regex condition" do
        params = {
          url: "https://www.google.com/",
          format: :json
        }
        headers = {'Content-Type' => 'application/json'}
        post "/shorten", params.to_json, headers
        expect(last_response.status).to eq 201
        json_response = JSON.parse(last_response.body)
        expect(json_response["shortcode"]).to match(Shortner::SHORT_CODE_REGEX)
      end
    end

    context "WHEN url have errors" do
      
      it "should return 400 for blank url" do
        shortcode = Shortner.gernerate_code
        params = {
          url: "",
          shortcode: shortcode,
          format: :json
        }
        headers = {'Content-Type' => 'application/json'}
        post "/shorten", params.to_json, headers
        expect(last_response.status).to eq 400
        json_response = JSON.parse(last_response.body)
        expect(json_response["error"]).to be == Shortner::URL_BLANK_MESSAGE
      end
      it "should return 400 for no present url" do
        shortcode = Shortner.gernerate_code
        params = {
          shortcode: shortcode,
          format: :json
        }
        headers = {'Content-Type' => 'application/json'}
        post "/shorten", params.to_json, headers
        expect(last_response.status).to eq 400
        json_response = JSON.parse(last_response.body)
        expect(json_response["error"]).to be == Shortner::URL_BLANK_MESSAGE
      end
    end

    context "WHEN shortcode does not match Regex", only: true do
      it "should return 422" do
        params = {
          url: "https://www.google.com/",
          shortcode: "AAAA()....",
          format: :json
        }
        headers = {'Content-Type' => 'application/json'}
        post "/shorten", params.to_json, headers
        expect(last_response.status).to eq 422
        json_response = JSON.parse(last_response.body)
        expect(json_response["error"]).to be == Shortner::SHORTCODE_FORMAT_MESSAGE
        
      end
    end
    
    context "WHEN shortcode already exists" do
      it "should return 409" do
        shortcode = Shortner.gernerate_code
        short_url = Shortner.new(url: "https://www.youtube.com/", shortcode: shortcode)
        expect(short_url.valid?).to be_truthy
        short_url.save  
        params = {
          url: "https://www.google.com/",
          shortcode: short_url.shortcode,
          format: :json
        }
        headers = {'Content-Type' => 'application/json'}
        post "/shorten", params.to_json, headers
        expect(last_response.status).to eq 409
        json_response = JSON.parse(last_response.body)
        expect(json_response["error"]).to be == Shortner::SHORTCODE_UNIQUE_MESSAGE
      end
    end
  end

  describe "/:shortcode" do
    context "WHEN the shortcode exists" do
      it "should return 302 and and the url in the location header" do
        shortcode = Shortner.gernerate_code
        short_url = Shortner.new(url: "https://www.youtube.com/", shortcode: shortcode)
        expect(short_url.valid?).to be_truthy
        short_url.save
        headers = {'Content-Type' => 'application/json'}
        params = {}
        get "/#{shortcode}", nil, headers
        expect(last_response.status).to eq 302
        expect(last_response.headers["Location"]).to  be == short_url.url
        json_response = JSON.parse(last_response.body)
        expect(json_response["url"]).to be == short_url.url
      end
    end
    context "WHEN the shortcode does not exists" do
      it "should return 404" do
        shortcode = Shortner.gernerate_code
        headers = {'Content-Type' => 'application/json'}
        params = {}
        get "/#{shortcode}", nil, headers
        expect(last_response.status).to eq 404
        json_response = JSON.parse(last_response.body)
        expect(json_response["error"]).to be == "#{shortcode} cannot be found in the system"
      end
    end
    
  end

  describe "/:shortcode/stats" do
    context "WHEN the shortcode exists and has been used" do
      context "AND has been used before" do
        it "should return 200 and and the stats of the code" do
          shortcode = Shortner.gernerate_code
          short_url = Shortner.new(url: "https://www.youtube.com/", shortcode: shortcode)
          short_url.redirect_count = 3
          short_url.created_at = 4.days.ago
          short_url.updated_at = 1.days.ago
          expect(short_url.valid?).to be_truthy
          short_url.save
          headers = {'Content-Type' => 'application/json'}
          params = {}
          get "/#{shortcode}/stats", nil, headers
          expect(last_response.status).to eq 200
          json_response = JSON.parse(last_response.body)
          expect(json_response["startDate"]).to be == short_url.created_at.iso8601
          expect(json_response["lastSeenDate"]).to be == short_url.updated_at.iso8601
          expect(json_response["redirectCount"]).to be == short_url.redirect_count
        end
      end
      context "AND never has been used before" do
        it "should return 200 and and the stats of the code" do
          shortcode = Shortner.gernerate_code
          short_url = Shortner.new(url: "https://www.youtube.com/", shortcode: shortcode)
          expect(short_url.valid?).to be_truthy
          short_url.save
          headers = {'Content-Type' => 'application/json'}
          params = {}
          get "/#{shortcode}/stats", nil, headers
          expect(last_response.status).to eq 200
          json_response = JSON.parse(last_response.body)
          expect(json_response["startDate"]).to be == short_url.created_at.iso8601
          expect(json_response["lastSeenDate"]).to be_nil
          expect(json_response["redirectCount"]).to be == short_url.redirect_count
        end
      end
    end
    context "WHEN the shortcode does not exists" do
      it "should return 404" do
        shortcode = Shortner.gernerate_code
        headers = {'Content-Type' => 'application/json'}
        params = {}
        get "/#{shortcode}", nil, headers
        expect(last_response.status).to eq 404
        json_response = JSON.parse(last_response.body)
        expect(json_response["error"]).to be == "#{shortcode} cannot be found in the system"
      end
    end
  end
end