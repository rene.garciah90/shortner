class Shortner 
  include MongoMapper::Document
  URL_BLANK_MESSAGE = "URL Can not be blank"
  SHORTCODE_BLANK_MESSAGE = "Shortcode can not be blank"
  SHORTCODE_FORMAT_MESSAGE = "Shortcode does not match required format"
  SHORTCODE_UNIQUE_MESSAGE = "has already been taken"
  REDIRECT_COUNT_ERROR_MESSAGE = "Redirect count can not be negative number"
  CODE_CHARACTERS = ["_","0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
  SHORT_CODE_REGEX = /^[0-9a-zA-Z_]{4,}$/
  key :url, String, required: {message: URL_BLANK_MESSAGE}
  key :shortcode, String, required: {message: SHORTCODE_BLANK_MESSAGE}, unique: {message: SHORTCODE_UNIQUE_MESSAGE}
  key :redirect_count, Integer, numeric: {message: REDIRECT_COUNT_ERROR_MESSAGE}, default: 0
  timestamps!
  attr_accessible :url, :shortcode
  validate :positive_number_on_count
  validate :url_not_blank
  validate :shortcode_not_blank
  validate :shortcode_regex
  
  def self.gernerate_code
    "#{CODE_CHARACTERS.sample(6).join}"
  end

  private

    def url_not_blank
      if self.url.to_s.strip.empty?
        errors.add( :url, URL_BLANK_MESSAGE)
      end
    end
    
    def shortcode_not_blank
      if self.shortcode_blank?
        errors.add( :shortcode, SHORTCODE_BLANK_MESSAGE)
      end
    end
    def shortcode_blank?
      self.shortcode.to_s.strip.empty?
    end
    
    def positive_number_on_count
      count = self.redirect_count || 0
      if count < 0
        errors.add( :redirect_count, REDIRECT_COUNT_ERROR_MESSAGE)
      end
    end

    def shortcode_regex
      if self.shortcode_blank? or !self.shortcode.match(SHORT_CODE_REGEX)
        errors.add( :shortcode, SHORTCODE_FORMAT_MESSAGE)
      end
    end
    
end
