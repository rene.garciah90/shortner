
require './configs/enviroment'
require 'sinatra'
require 'sinatra/json'
require './models/shortner'

configure do
  connection_config = {
    "production" => { 'uri' =>  ENV['PROD_MONGODB_URI'] },
    "development" => {'uri' =>  ENV['DEV_MONGODB_URI'] },
    "test" => { 'uri' =>  ENV['TEST_MONGODB_URI'] }
  }
  
  MongoMapper.setup(connection_config, @env)
end

get '/' do
  "Hello world, this is shortly app at #{Time.now}"
end

post '/shorten', provides: :json do
  pass unless request.accept? 'application/json'
  content_type 'application/json'
  json_payload = request.body.read
  params = JSON.parse(json_payload).transform_keys(&:to_sym)
  logger.info "Body Params => #{params}"
  url = params[:url]
  shortcode = params[:shortcode] || Shortner.gernerate_code
  short_url = Shortner.new(url: url, shortcode: shortcode)
  if(short_url.valid?)
    short_url.save
    status 201
    json shortcode: short_url.shortcode
  else
    case 
    when (short_url.errors.has_key?(:url) and short_url.errors[:url].include?(Shortner::URL_BLANK_MESSAGE))
      status 400
      json error: Shortner::URL_BLANK_MESSAGE
    when short_url.errors.has_key?(:shortcode)
      case 
      when short_url.errors[:shortcode].include?(Shortner::SHORTCODE_BLANK_MESSAGE)
        status 400
        json error: Shortner::SHORTCODE_BLANK_MESSAGE
      when short_url.errors[:shortcode].include?(Shortner::SHORTCODE_UNIQUE_MESSAGE)
        status 409
        json error: Shortner::SHORTCODE_UNIQUE_MESSAGE
      when short_url.errors[:shortcode].include?(Shortner::SHORTCODE_FORMAT_MESSAGE)
        status 422
        json error: Shortner::SHORTCODE_FORMAT_MESSAGE
      else
        status 400
        json errors: short_url.errors
      end
    else
      
    end    
  end
end

get '/:shortcode', provides: :json do
  pass unless request.accept? 'application/json'
  content_type 'application/json'
  short_url = Shortner.first(shortcode: params["shortcode"])
  if(short_url.present?)
    short_url.increment(redirectCount: 1)
    status 302
    headers "Location" => short_url.url
    json url: short_url.url
  else
    status 404
    json error: "#{params["shortcode"]} cannot be found in the system"
  end
end

get '/:shortcode/stats', provides: :json do
  pass unless request.accept? 'application/json'
  content_type 'application/json'
  short_url = Shortner.first(shortcode: params[:shortcode])
  if(short_url.present?)
    json_response = if short_url.redirect_count > 0
      {
        startDate: short_url.created_at.iso8601,
        lastSeenDate: short_url.updated_at.iso8601,
        redirectCount: short_url.redirect_count
      }
    else
      {
        startDate: short_url.created_at.iso8601,
        redirectCount: short_url.redirect_count
      }
    end
    json json_response
  else
    status 404
    json error: "#{params["shortcode"]} cannot be found in the system"
  end
end